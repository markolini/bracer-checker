OPENED_PARENTHESIS = '({['
CLOSED_PARENTHESIS = ')}]'

BALANCED = 'Expression is balanced'
NOT_BALANCED = 'Expression is not balanced'
EMPTY = 'Expression is empty'

FILE_BALANCED = 'expressions/balanced'
FILE_NOT_BALANCED = 'expressions/not_balanced'
FILE_EMPTY_EXPRESSION = 'expressions/empty_expression'
FILE_BRACERS_ONLY_BALANCED = 'expressions/bracers_only_balanced'
FILE_BRACERS_ONLY_NOT_BALANCED = 'expressions/bracers_only_not_balanced'
FILE_LETTERS_ONLY = 'expressions/letters_only'