def map_parenthesis(opened_parenthesis, closed_parenthesis):
    """
    Maps the opened and closed parenthesis into a dictionary
    with keys being the opened parenthesis and values being
    closed parenthesis.
    """
    return dict(zip(opened_parenthesis, closed_parenthesis))


def expression_empty(expression):
    """ Checks if the given expression is empty. """
    return len(expression) == 0


def stack_empty(stack):
    """ Checks if given stack is empty. """
    return not stack


def wrong_bracer_popped(character, stack):
    """
    Checks if the wrong bracer has been popped from
    the given stack.
    """
    return not character == stack.pop()


def balanced(expression, stack, mapping):
    """ Checks the balance of the expression. """
    for character in expression:
        if character in mapping.keys():
            stack.append(mapping[character])
        elif character in mapping.values():
            if stack_empty(stack) or wrong_bracer_popped(character, stack):
                return False
    return True


def check_expression(expression):
    """
    This method checks if the given expression is balanced.
    Ignores any other characters unless its a parenthesis.
    Expression containing no parenthesis is considered balanced.
    Empty expression is not checked.
    """
    import constant as const

    # Expression length check
    if expression_empty(expression):
        return const.EMPTY

    # Mapping parenthesis
    mapping = map_parenthesis(const.OPENED_PARENTHESIS, const.CLOSED_PARENTHESIS)
    stack = []

    # Balance check
    if not balanced(expression, stack, mapping):
        return const.NOT_BALANCED
    else:
        return const.BALANCED


def check_file(path):
    """
    Opens a file on a given file path and extracts
    content of the file for checking.
    """
    # Opening a file
    file = open(path)

    # Reading content
    content = file.read()

    # Checking expression
    print(check_expression(content))


def main():
    import constant as const

    check_file(const.FILE_BALANCED)
    check_file(const.FILE_NOT_BALANCED)
    check_file(const.FILE_EMPTY_EXPRESSION)
    check_file(const.FILE_BRACERS_ONLY_BALANCED)
    check_file(const.FILE_BRACERS_ONLY_NOT_BALANCED)
    check_file(const.FILE_LETTERS_ONLY)


main()
